

rand_init: {
    lda #$7f
    sta $dc04
    lda #$33
    sta $dc05
    lda #$2f
    sta $dd04
    lda #$79
    sta $dd05
    lda #$91
    sta $dc0e
    sta $dd0e
    rts
}

rand_get: {
    lda seed
    beq doEor
    asl
    beq noEor
    bcc noEor
doEor:    
    eor #$1d
    eor $dc04
    eor $dd04
noEor:  
    sta seed
    rts
seed:
    .byte $62
}

// Get a random number 0 <= x < 40
rand_x: {
    jsr rand_get
    ora #$3f
    cmp #40
    bcc !+
    clc
    sbc #40
 !: rts
}

// Get a random number 0 <= x < 25
rand_y: {
    jsr rand_get
    ora #$1f
    cmp #25
    bcc !+
    clc
    sbc #25
 !: rts
}
  
