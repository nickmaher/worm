* = $02 "Zeropage" virtual

.label DIR_NONE = 0
.label DIR_UP = 1
.label DIR_DOWN = 2
.label DIR_LEFT = 3
.label DIR_RIGHT = 4

.label KEY_UP = 9
.label KEY_DOWN = 13
.label KEY_LEFT = 10
.label KEY_RIGHT = 18

.label CHR_HEAD = $53
.label CHR_BODY = $51
.label CHR_APPLE = $55
.label CHR_SPACE = $20

PlayerHeadX:    .byte $00
PlayerHeadY:    .byte $00
PlayerTailX:    .byte $00
PlayerTailY:    .byte $00
PlayerDir:      .byte $00
PlayerGrow:     .byte $00
AppleX:         .byte $00
AppleY:         .byte $00

Tmp0: .byte $00
Tmp1: .byte $00
Tmp2: .byte $00
TmpX: .byte $00
TmpY: .byte $00

.label SCREEN = $0400
.label DIRECTION = $8000
.label KEYBOARD = $c5

BasicUpstart2(start)

* = $4000 "Main Program"
#import "rand.asm"

start:  
    lda #$00
    sta $d020
    jsr clear
    jsr rand_init

    lda #$03
    sta PlayerHeadY
    sta PlayerTailY
    sta PlayerHeadX
    sta PlayerTailX
    lda #DIR_DOWN
    sta PlayerDir
    lda #$03
    sta PlayerGrow

    jsr draw_apple

next:
    jsr move_player
    jsr check_keyboard
    jsr wait
    jmp next

// Wait for 4 screen refreshes
wait: {    
    ldy #$04
loop1:
    lda #$fb
    cmp $d012
    bne loop1
loop2:    
    cmp $d012
    beq loop2
    dey
    bne loop1
    rts
}

draw_apple: {
    jsr rand_x
    sta AppleX
    tax
    jsr rand_y
    sta AppleY
    tay
    jsr get_char
    cmp #CHR_SPACE
    bne draw_apple
    ldx AppleX
    ldy AppleY
    lda #CHR_APPLE
    jsr plot
    rts
}

eat_apple: {
    jsr draw_apple
    lda PlayerGrow
    adc #$03
    sta PlayerGrow
    rts
}

move_player: {

    ldy PlayerDir

    // Adjust the current X position according to the current direction.
    // If it's out of bounds, die. Save the adjusted X position in TmpX.
    clc
    lda TABLES.DirDeltaX,y
    adc PlayerHeadX
    cmp #40
    bcs die
    sta TmpX

    // Adjust the current Y position according to the current direction.
    // If it's out of bounds, die. Save the adjusted Y position in TmpY.
    clc
    lda TABLES.DirDeltaY,y
    adc PlayerHeadY
    cmp #25
    bcs die
    sta TmpY

    // Check the character in the new position
    ldx TmpX
    ldy TmpY
    jsr get_char
    cmp #CHR_APPLE
    bne !+
    jsr eat_apple
    jmp blah
!:  cmp #CHR_SPACE
    bne die

blah:
    // Store the player position in the direction buffer
    lda PlayerDir  
    ldx PlayerHeadX
    ldy PlayerHeadY
    jsr set_dir

    // Plot the body
    ldx PlayerHeadX
    ldy PlayerHeadY
    lda #CHR_BODY
    jsr plot

    // Plot the new head
    ldx TmpX
    stx PlayerHeadX
    ldy TmpY
    sty PlayerHeadY
    lda #CHR_HEAD
    jsr plot

    // If the grow count is non-zero, decrease it and we're done.
    ldx PlayerGrow
    beq trim
    dex
    stx PlayerGrow
    rts

trim:
    // Get the direction from the tail
    ldx PlayerTailX
    ldy PlayerTailY
    jsr get_dir
    tay

    // Calculate the new tail position
    clc
    lda TABLES.DirDeltaX,y
    adc PlayerTailX
    sta TmpX
    clc
    lda TABLES.DirDeltaY,y
    adc PlayerTailY
    sta TmpY

    // Delete the tail from the screen
    ldx PlayerTailX
    ldy PlayerTailY
    lda #CHR_SPACE
    jsr plot
    
    // Update the tail pointer
    lda TmpX
    sta PlayerTailX
    lda TmpY
    sta PlayerTailY
    rts

die:
    lda #$00
    sta $d020
    lda #DIR_NONE
    sta PlayerDir
    jmp die
}

check_keyboard: {

    lda KEYBOARD

    cmp #KEY_UP
    bne !+
    lda PlayerDir
    cmp #DIR_DOWN
    beq done
    lda #DIR_UP
    sta PlayerDir
    rts

!:  cmp #KEY_DOWN
    bne !+
    lda PlayerDir
    cmp #DIR_UP
    beq done
    lda #DIR_DOWN
    sta PlayerDir
    rts

!:  cmp #KEY_LEFT
    bne !+
    lda PlayerDir
    cmp #DIR_RIGHT
    beq done
    lda #DIR_LEFT
    sta PlayerDir
    rts

!:  cmp #KEY_RIGHT
    bne !+
    lda PlayerDir
    cmp #DIR_LEFT
    beq done
    lda #DIR_RIGHT
    sta PlayerDir
    rts

!:
done:
    rts
}

clear: { 
    lda #$20       
    ldx #$00
!:  sta SCREEN + 000, x
    sta SCREEN + 250, x
    sta SCREEN + 500, x
    sta SCREEN + 750, x
    inx
    cpx #250
    bne !-
    rts
}

// Plot the character in A on the screen at X,Y.
// X and Y are not preserved.
plot: {
    pha
    lda TABLES.ScreenRowLSB,y
    sta Tmp0
    lda TABLES.ScreenRowMSB,y
    sta Tmp1
    txa
    tay
    pla
    sta (Tmp0), y
    rts
}

get_char: {
    lda TABLES.ScreenRowLSB,y
    sta Tmp0
    lda TABLES.ScreenRowMSB,y
    sta Tmp1
    txa
    tay
    lda (Tmp0), y
    rts
}

set_dir: {
    pha
    lda TABLES.DirBufRowLSB,y
    sta Tmp0
    lda TABLES.DirBufRowMSB,y
    sta Tmp1
    txa
    tay
    pla
    sta (Tmp0), y
    rts
}

get_dir: {
    lda TABLES.DirBufRowLSB,y
    sta Tmp0
    lda TABLES.DirBufRowMSB,y
    sta Tmp1
    txa
    tay
    lda (Tmp0), y
    rts
}

TABLES: {
    ScreenRowLSB:
        .fill 25, <[SCREEN + i * 40]
    ScreenRowMSB:
        .fill 25, >[SCREEN + i * 40]
    DirBufRowLSB:
        .fill 25, <[DIRECTION + i * 40]
    DirBufRowMSB:
        .fill 25, >[DIRECTION + i * 40]
    DirDeltaX:
        .byte $00, $00, $00, $FF, $01
    DirDeltaY:
        .byte $00, $FF, $01, $00, $00
}

// So the game will be basically 'Deathworm'.
// Two players each controlling a worm
// We'll need either joystick control or better keyboard scanning code
// Does the C64 even support N-key rollover?

// For each player:
// HeadX
// HeadY
// TailX
// TailY
// Direction (1-4)
// GrowCount
// 6 bytes per player.

// Move logic
// ----------
// Keyboard logic
// --------------
// Convert scancode to a direction (1-4)
// Or zero if we didn't get one
// Make sure it's a valid direction change.
// Set the new direction

// Store the player vars in zero page?
// Maybe do this, and we can use zero page indexed addressing

// Store the direction in DirectionBuf[HeadX,HeadY]
// Plot body at HeadX/Head/Y
// Adjust HeadX/HeadY
// Check to see if HeadX/HeadY contains a snake.. if so set dead flag
// Plot head at HeadX/HeadY
// If GrowCount > 0, then decrease it.
// Otherwise, trim the tail:
// - Plot a space at TailX,TailY
// - Get the direction from DirectionBuf[TailX, TailY]
// - Switch on the direction, and adjust TailX or TailY

